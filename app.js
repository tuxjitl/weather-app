const request = require('request');

const url =
  'https://api.darksky.net/forecast/f69039a6aaa891fd057539d4742de5d9/37.8267,-122.4233?units=si&lang=nl';

request({ url: url, json: true }, (error, response) => {
  if (error) {
    //geen connectie
    console.log('Unable to connect to weather service');
  } else if (response.body.error) {
    //geen locatie gevonden
    console.log('Unable to find location');
  } else {
    console.log(response.body.currently.summary);
    console.log(
      'It is now ' +
        response.body.currently.temperature +
        ' degrees' +
        ' and there is a ' +
        response.body.currently.precipProbability +
        '% change of rain.'
    );
  }
});

// GEOcoding
const urlGeo =
  'https://api.mapbox.com/geocoding/v5/mapbox.places/Genk.json?access_token=pk.eyJ1IjoiamNsMTk2OSIsImEiOiJjazg1a3c3bzEwN2djM2dvN3JoZ251aTY3In0.yNo8IFJjxkvFcaEfEUMvjQ&language=nl&limit=1';

request({ url: urlGeo, json: true }, (error, response) => {
  if (error) {
    //geen connectie
    console.log('Unable to connect to weather GEO location service');
  } else if (response.body.features.length === 0) {
    //geen locatie gevonden
    console.log('Unable to find location');
  } else {
    const latitude = response.body.features[0].center[1];
    const longitude = response.body.features[0].center[0];
    console.log('Latitude: ', latitude, '\t', 'Longitude: ', longitude);
  }
});
